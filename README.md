East Windsor is an ideal location for independent senior living with quick access to major highways and other local amenities. Find shopping, dining, and entertainment within walking distance of Connect55+ East Windsor!

Address: 20 North Road, East Windsor, CT 06088, USA

Phone: 860-249-0974

Website: [https://www.connect55.com/communities/east-windsor](https://www.connect55.com/communities/east-windsor)
